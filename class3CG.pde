// intention of class activity - practice isolating transformations through push matrix and pop matrix
float generalAngle, d11, d12, d21, d22;

void setup() {

  size(800, 800, P3D);
  
  // color of the lines in the geometry
  stroke(255);
  
  // no fill - just lines
  noFill();
  
  generalAngle = d11 = d12 = d21 = d22 = 0;
}

void draw() {

  background(0);
  
  //drawDog();
  hand();
}

void drawDog() {
  
  // no requirements whatsoever regarding specific transformations - this will change for the exam
  translate(width/2, height/2, 0);
  
  // WHEN TO PUSH
  // when we know that the transformations that will be applied must not be applied to anyone else that is not
  // part of the hierarchy
  
  // when we push we save the current state of the matrix into a stack
  pushMatrix();
  scale(2, 1, 1);
  box(100);
  
  popMatrix();
  // reestablish the matrix to its previous state
  
  
  translate(80, -80, 0);
  
  // not set in stone - push every time that you do transformation (THIS IS NOT A REQUIREMENT)
  // evaluate which ones you want to keep and which don't 
  
  pushMatrix();
  scale(0.5, 1, 1);
  box(50);
  popMatrix();
  
  // last one
  // not required to push / pop
  translate(0, -70, 0);
  scale(2, 1, 1);
  box(70);
}

void hand() {
  
  if(keyPressed) {
    
    // rotation of the whole hand
    if(key == 'r') generalAngle += 0.01f;
    if(key == 'f') generalAngle -= 0.01f;
    
    // rotation of the base of the first finger
    if(key == '1') d11 += 0.01f;
    if(key == 'q') d11 -= 0.01f;
    // rotation of the tip of the first finger
    if(key == 'a') d12 += 0.01f;
    if(key == 'z') d12 -= 0.01f;
    
    // rotation of the base of the second finger
    if(key == '2') d21 += 0.01f;
    if(key == 'w') d21 -= 0.01f;
    // rotation of the tip of the second finger
    if(key == 's') d22 += 0.01f;
    if(key == 'x') d22 -= 0.01f;
  }
  
  translate(width/2, height/2, 0);
  rotateY(generalAngle);
  
  // draw the main part
  pushMatrix();
  scale(1.5, 1.5, 1);
  box(100);
  popMatrix();

  // solution: isolate the finger 
  pushMatrix();
  // this is the first finger
  translate(-50, -75, 0);
  rotateX(d11);
  translate(0, -50, 0);
  
  
  pushMatrix();
  scale(1, 2, 1);
  box(50);
  popMatrix();
  
  translate(0, -50, 0);
  rotateX(d12);
  translate(0, -50, 0);
  
  pushMatrix();
  scale(1,2,1);
  box(50);
  popMatrix();
  
  popMatrix();
  
  // solution: isolate the finger 
  pushMatrix();
  // this is the second finger
  translate(50, -75, 0);
  rotateX(d21);
  translate(0, -50, 0);
  
  
  pushMatrix();
  scale(1, 2, 1);
  box(50);
  popMatrix();
  
  translate(0, -50, 0);
  rotateX(d22);
  translate(0, -50, 0);
  
  pushMatrix();
  scale(1,2,1);
  box(50);
  popMatrix();
  
  popMatrix();
}
